# GitLab template - Sphinx

This template was developed by [Bio-IT community](https://bio-it.embl.de/) under the [Open Software License](/LICENSE.md).
For a full introduction on how to use and customise these templates, check
[this lesson](https://grp-bio-it-workshops.embl-community.io/building-websites-with-gitlab/).

**Pushing** this project to its remote repository triggers a **pipeline** to generate a static website from the
**reStructuredText** files in the main folder, converted to HTML in `public`.
These instructions are specified in the `.gitlab-ci.yml` file.

Learn more about [GitLab Pages](https://about.gitlab.com/product/pages/) by consulting the official
[documentation](https://docs.gitlab.com/ee/user/project/pages/).
