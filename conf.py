# -- Project information -----------------------------------------------------

project = 'My project'
copyright = '2021, Lisanna Paladin'
author = 'Lisanna Paladin'
release = '1.0'

# -- Options for HTML output -------------------------------------------------

html_theme = 'alabaster'